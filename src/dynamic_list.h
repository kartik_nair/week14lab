#include <memory.h>

#include <ostream>

/**
 * @brief Dynamically sized vector-like list implementation.
 * @tparam T The type of the elements.
 */
template <typename T>
class DynamicList {
   private:
    T* _arr;
    size_t capacity;

   public:
    size_t length;

    DynamicList() {
        length = 0;
        capacity = 8;
        _arr = new T[capacity];
    }
    DynamicList(std::initializer_list<T> list) {
        length = list.size();
        capacity = length;
        _arr = new T[capacity];
        std::copy(list.begin(), list.end(), _arr);
    }
    ~DynamicList() { delete[] _arr; }

    T operator[](int index) { return get(index); }

    T get(int index) const {
        if (index < 0 || (size_t)index >= length) {
            throw std::runtime_error("Accessing out of bounds");
        }

        return _arr[index];
    }

    void add(const T& value) {
        // we've hit the cap
        if (length == capacity) {
            size_t old_capacity = capacity;
            capacity = capacity * 2;

            T* old_arr = _arr;
            _arr = new T[capacity];

            // copy the memory of the old elements
            // into the new larger container
            memcpy(_arr, old_arr, old_capacity * sizeof(T));
        }

        _arr[length] = value;
        length++;
    }

    void remove(int index) {
        if (index < 0 || (size_t)index > length) {
            throw std::runtime_error("Accessing out of bounds");
        }

        length--;

        if (length == 0) {
            delete[] _arr;
        } else {
            for (size_t i = index; i < capacity; i++) _arr[i] = _arr[i + 1];
        }
    }

    T pop() {
        T temp = _arr[length - 1];
        remove(length - 1);
        return temp;
    }
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const DynamicList<T>& l) {
    os << '[';
    size_t last = l.length - 1;
    for (size_t i = 0; i <= last; ++i) {
        os << l.get(i);
        if (i != last) os << ", ";
    }
    os << "]";
    return os;
}
