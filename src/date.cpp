#include "date.h"

Date::Date(int day, int month, int year) : day(day), month(month), year(year){};

int Date::get_day() const { return day; }

int Date::get_month() const { return month; }

int Date::get_year() const { return year; }

std::ostream& operator<<(std::ostream& os, const Date& date) {
    os << date.get_day() << '-' << date.get_month() << '-' << date.get_year();
    return os;
}

bool operator<(const Date& lhs, const Date& rhs) {
    if (lhs.get_year() < rhs.get_year()) {
        return true;
    } else if (lhs.get_year() == rhs.get_year() &&
               lhs.get_month() < rhs.get_month()) {
        return true;
    } else if (lhs.get_year() == rhs.get_year() &&
               lhs.get_month() == rhs.get_month() &&
               lhs.get_day() < rhs.get_day()) {
        return true;
    }

    return false;
}