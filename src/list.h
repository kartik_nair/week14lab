#include <ostream>

template <typename T, int MaxSize>
class List {
   private:
    T* _arr;

   public:
    size_t length;

    List() {
        length = 0;
        _arr = new T[MaxSize];
    }
    ~List() { delete[] _arr; }

    T operator[](int index) { return get(index); }

    T get(int index) const {
        if (index < 0 || (size_t)index >= length) {
            throw std::runtime_error("Accessing out of bounds.");
        }

        return _arr[index];
    }

    void add(const T& value) {
        // we've hit the cap
        if (length > MaxSize) {
            throw std::runtime_error("List is full.");
        }

        _arr[length] = value;
        length++;
    }

    void remove(int index) {
        if (index < 0 || (size_t)index > length) {
            throw std::runtime_error("Accessing out of bounds.");
        }

        length--;

        if (length == 0) {
            delete[] _arr;
        } else {
            for (size_t i = index; i < length; i++) _arr[i] = _arr[i + 1];
        }
    }

    size_t size() { return length; }
};

template <typename T, int MaxSize>
std::ostream& operator<<(std::ostream& os, const List<T, MaxSize>& l) {
    os << '[';
    size_t last = l.length - 1;
    for (size_t i = 0; i <= last; ++i) {
        os << l.get(i);
        if (i != last) os << ", ";
    }
    os << "]";
    return os;
}
