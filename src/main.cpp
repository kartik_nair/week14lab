#include <iostream>
#include <sstream>

#include "date.h"
#include "dynamic_list.h"
#include "list.h"

int main() {
    Date today(25, 1, 2021);
    std::cout << "Today is: " << today << std::endl;
    Date tomorrow(26, 1, 2021);
    std::cout << "Tomorrow is: " << tomorrow << std::endl;
    Date next_year(1, 1, 2022);
    std::cout << "Next year is: " << next_year << std::endl;

    std::cout << "\n\n[some comparisons]\n" << std::endl;

    std::cout << "today < tomorrow? " << (today < tomorrow) << std::endl;
    std::cout << "tomorrow < today? " << (tomorrow < today) << std::endl;
    std::cout << "tomorrow < next_year? " << (tomorrow < next_year)
              << std::endl;
    std::cout << "next_year < tomorrow? " << (next_year < tomorrow)
              << std::endl;

    std::ostringstream ss;
    ss << today << '\n' << tomorrow << '\n' << next_year << std::endl;
    std::cout << "stream is:\n" << ss.str();

    std::cout << "\n\n[list stuff]\n" << std::endl;

    // DynamicList<int> l = {24, 52, 12};

    List<int, 100> l;
    l.add(24);
    l.add(52);
    l.add(12);

    std::cout << "List has length: " << l.length << std::endl;
    std::cout << "List 0: " << l[0] << std::endl;
    std::cout << "List 1: " << l[1] << std::endl;
    std::cout << "List 2: " << l[2] << std::endl;

    l.remove(1);
    std::cout << "\n\n[removed second element]\n" << std::endl;
    std::cout << "List has length: " << l.length << std::endl;
    std::cout << "List 0: " << l[0] << std::endl;
    std::cout << "List 1: " << l[1] << std::endl;

    l.remove(0);
    l.remove(1);
    std::cout << "\n\n[removed all elements]\n" << std::endl;
    std::cout << "List has length: " << l.length << std::endl;
}