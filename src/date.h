#include <ostream>

class Date {
   private:
    int day;
    int month;
    int year;

   public:
    Date(int, int, int);
    int get_day() const;
    int get_month() const;
    int get_year() const;
};

std::ostream& operator<<(std::ostream& os, const Date& date);
bool operator<(const Date& lhs, const Date& rhs);
